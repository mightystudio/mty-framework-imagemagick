## Description
This is core library of modules and methods for common use of ImageMagick library to be used across different apps

## Documentation
This repository is a part of the "Mighty - Metacube" Pipeline Toolkit.

- For more information about this app and for release notes, *see the wiki section*.
- For information about Shotgun Toolkit in general, click here: http://www.shotgunsoftware.com/toolkit
- This repository uses semantic versioning, please check more about it here: https://semver.org/

## Using this framework in your Setup
All the frameworks that are part of our standard framework suite are pushed directly to our project configs

If you need to manually add this to a project config, you should use the bondle and payload workflow for clowd configs
To know more about this you can check the following documentation and webinar:

- Cloud Configs Webinar:
  - https://www.youtube.com/watch?v=NyIk07F2RoM
- Developer docs on Toolkit Initialization:
  - https://developer.shotgunsoftware.com/tk-core/initializing.html
- Developer docs on Toolkit Descriptors:
  - https://developer.shotgunsoftware.com/tk-core/descriptor.html#descriptor-types

The basics are to create the app configuration as follows:

In this case, under frameworks.yml
```yaml
  mty-framework-imagemagick:
    location:
      type: shotgun
      entity_type: CustomNonProjectEntity10
      name: mty-framework-imagemagick
      version: ---
      field: sg_payload
```

CustomNonProjectEntity10 is the Shotgun custom entity to be used as the Toolkit Bundle mechanism.
The version number is the id of the attachment entity under the sg_payload field for the corresponding bundle.

## Using Framework code from other apps

To be able to use code from a framework, you must make sure that teh framework is listed as a requierement for your app.
To do so, you should ass in under the info.yml file under the frameworks section:

```yaml
frameworks:
    - {"name": "mty-framework-imagemagick", "version": "v---"}
```

Note that its crucial to specify the version number with a "v" as a prefix and matching the exact attachment id for that version

Then, from your app code, you just load the module using app's frameworks property:
```python
  class CustomApplication(Application):

    def init_app(self):

      imagemagickTools = self.frameworks.get("mty-framework-imagemagick")
      self.ImageMagickCoreTools = imagemagickTools.ImageMagickCoreTools
```

## Using Framework code from hooks or Engines

If you are at a hook or angine level and need to access arbitrary code from any framework, you can do so by using the import_framework method from the sgtk's platform module:
```python
# option one
import sgtk
ImageMagickCoreTools = sgtk.platform.import_framework("mty-framework-imagemagick", "ImageMagickCoreTools")

# option two
imagemagick = self.load_framework("mty-framework-imagemagick_v---")
ImageMagickCoreTools = imagemagick.imageMagickCore
```