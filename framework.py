#####################################################################################
#
# Copyright (c) 2020 Mighty Animation Studio and Metacube Technology Entertainment
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided as part of a colaboration and subject to the
# Mighty - Metacube Pipeline Code Agreement and your personal work agreement.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the the specific terms of your contract. All rights
# not expressly granted therein are reserved by Mighty Animation Studio and
# Metacube Technology entertainment.
#
#####################################################################################

"""
A framework that handles uploading and downloading of files from a cloud storage provider.
"""

import sgtk


class ImageMagickTools(sgtk.platform.Framework):

    def init_framework(self):
        """
        Construction
        """
        
        # add modules to this instance so the interface is nicer for users
        imageMagickCore = self.import_module("imageMagickCoreTools")

        self.imageMagickCore = imageMagickCore.ImageMagickCoreTools()
        